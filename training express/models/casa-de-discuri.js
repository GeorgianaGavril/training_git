module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "caseDeDiscuri",
    {
      denumire: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      tableName: "caseDeDiscuri",
    }
  );
};
