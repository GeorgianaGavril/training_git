const { DataTypes } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    return sequelize.define(
        'manea',
        {
            denumire: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            artist: {
                type: DataTypes.STRING,
                allowNull: false,
            },
        },
        {
            tableName: "manele",
        }
    );
}
