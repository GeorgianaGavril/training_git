const Sequelize = require('sequelize');
const db = require('../config/db');

const CaseDeDiscuriModel = require("./casa-de-discuri");
const ManeaModel = require('./manea');

const CaseDeDiscuri = CaseDeDiscuriModel(db, Sequelize);
const Manea = ManeaModel(db, Sequelize);

CaseDeDiscuri.hasMany(Manea,  {
    foreignKey: "caseDeDiscuriId",
    as: "Manele",
    onDelete: "CASCADE",
});
Manea.belongsTo(CaseDeDiscuri);

module.exports = {
    CaseDeDiscuri,
    Manea,
    connection: db,
};