const express = require("express");
const router = require('./routes');

const app = express();
app.use(express.json());

const port = 8080;

app.use('/api', router);

app.use("/", (req, res) => {
    res.status(200).send({message: "Aplicatia ruleaza"});
});

app.listen(port, () => {
    console.log(`Aplicatia ruleaza pe portul ${port}`);
});
