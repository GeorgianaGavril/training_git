const { casaDeDiscuri } = require('.');

const CaseDeDiscuriDb = require('../models').CaseDeDiscuri;
const ManeaDb = require('../models').Manea;

const controller = {
    getAllCaseDeDiscuri: (req, res) => {
        CaseDeDiscuriDb.findAll({include: [{model: ManeaDb, as: "Manele"}]})
        .then((CaseDeDiscuri) => {
            res.status(200).send(CaseDeDiscuri);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({message: 'Server error!'})
        })
    },

    addCasaDeDiscuri: (req, res) => {
        const { denumire } = req.body;
        CaseDeDiscuriDb.create({denumire})
        .then((casaDeDiscuri) => {
            res.status(201).send(casaDeDiscuri);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({message: 'Eroare la adaugarea casei de discuri'})
        })
    },

    //tema
    
    getCasaDeDiscuriById: (req, res) => {
        const { id } = req.params;
        if(!id) {
            res.status(400).send({message: "Trebuie specificat un id"})
        }

        CaseDeDiscuriDb.findByPk(id)
        .then((casaDeDiscuri) => {
            if(casaDeDiscuri) {
                res.status(200).send(casaDeDiscuri);
            }
            else {
                res.status(404).send({message: 'Casa de discuri nu a fost gasita!'});
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server error!"});
        });
    },

    updateCasaDeDiscuriById: (req, res) => {
        const { id } = req.params;
        if (!id) {
            res.status(400).send({ message: "Trebuie sa specifici id-ul!"});
        }

        const { denumire } = req.body;
        if (!denumire) {
            res
              .status(400)
              .send({ message: "Trebuie sa specifici minim o modificare!"});
        } else {
            CaseDeDiscuriDb.findByPk(id)
              .then(async (casaDeDiscuri) => {
                if (casaDeDiscuri) {
                    Object.assign(casaDeDiscuri, req.body);
                    await casaDeDiscuri.save();
                    res
                      .status(202)
                      .send({ message: "Casa de discuri a fost actualizata cu succes!"});
                } else {
                    res.status(404).json({ message: "Casa de discuri nu exista!"});
                }
              })
              .catch((err) => {
                console.error(err);
                res.status(500).send({ message: "Eroare de server!" });
              });
        }
    },

    deleteCasaDeDiscuriById: (req, res) => {
        const { id } = req.params;
        if (!id) {
            res.status(400).send({ message: "Trebuie sa specifici id-ul!"});
        }

        CaseDeDiscuriDb.findByPk(id)
          .then(async (casaDeDiscuri) => {
            if (casaDeDiscuri) {
                await casaDeDiscuri.destroy();
                res
                  .status(202)
                  .send({ message: "Casa de discuri a fost stearsa cu succes!"});
            } else {
                res.status(404).json({ message: "Casa de discuri nu exista!"});
            }
          })
          .catch((err) => {
             console.error(err);
             res.status(500).send({ message: "Eroare de server! "});
          });
    }
}
module.exports = controller;