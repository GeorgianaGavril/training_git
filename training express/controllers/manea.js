const casaDeDiscuri = require('../models/casa-de-discuri');

const CaseDeDiscuriDb = require('../models').CaseDeDiscuri;
const ManeaDb = require('../models').Manea;

const controller = {
    getAllManele: (req, res) => {
        ManeaDb.findAll()
        .then((manele) => {
            res.status(200).send(manele);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server Error!"});
        });
    },

    getManeaById: (req, res) => {
        const { id } = req.params;
        if(!id) {
            res.status(400).send({message: 'Trebuie specificat un id'})
        }

        ManeaDb.findByPk(id)
        .then((manea) => {
            if(manea) {
                res.status(200).send(manea);
            }
            else {
                res.status(404).send({message: "Maneaua nu a fost gasita"})
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server error!" });
        });
    },

    addManea: (req, res) => {
        const { caseDeDiscuriId, denumire, artist } = req.body;
        CaseDeDiscuriDb.findByPk(caseDeDiscuriId)
        .then((casaDeDiscuri) => {
            if(casaDeDiscuri) {
                casaDeDiscuri.createManele({denumire, artist})
                .then((manea) => {
                    res.status(201).send(manea);
                })
                .catch((err) => {
                    console.log(err);
                    res.status(500).send({message: 'Server Error!'});
                })
            }
            else {
                res.status(404).send({message: 'Casa de discuri nu exista'})
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({message: 'Server Error!'});
        });
    },

    updateManea: (req, res) => {
    const { id } = req.params;
    if (!id) {
      res.status(400).send({ message: "Trebuie sa specifici id-ul!" });
    }

    const { denumire, artist } = req.body;
    if (!denumire && !artist) {
      res
        .status(400)
        .send({ message: "Trebuie sa specifici minim o modificare!" });
    } else {
      ManeaDb.findByPk(id)
        .then(async (manea) => {
          if (manea) {
            Object.assign(manea, req.body);
            await manea.save();
            res
              .status(202)
              .send({ message: "Maneaua a fost actualizata cu succes!" });
          } else {
            res.status(404).json({ message: "Maneaua nu exista!" });
          }
        })
        .catch((err) => {
          console.error(err);
          res.status(500).send({ message: "Eroare de server!" });
        });
    }
  },

    //tema

    deleteManeaById: (req, res) => {
        const { id } = req.params;
        if (!id) {
            res.status(400).send({ message: "Trebuie sa specifici un id!"});
        }

        ManeaDb.findByPk(id)
        .then(async (manea) => {
          if (manea) {
              await manea.destroy();
              res
                .status(202)
                .send({ message: "Maneaua a fost stearsa cu succes!" });
          } else {
              res.status(404).json({ message: "Maneaua nu exista!"});
          }
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send({ message: "Eroare de server! "});
        });
    }
}

module.exports = controller;