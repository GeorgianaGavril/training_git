const express = require('express');
const router = express.Router();
const otherRouter = require('./other');
const casaDeDiscuriRouter = require('./casa-de-discuri');
const maneaRouter = require('./manea');

router.use('/', otherRouter);
router.use("/caseDeDiscuri", casaDeDiscuriRouter);
router.use("/manele", maneaRouter);

module.exports = router;
